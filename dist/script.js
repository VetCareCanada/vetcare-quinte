/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/script.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/blocks/_wp-block-cover.js":
/*!******************************************!*\
  !*** ./src/js/blocks/_wp-block-cover.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  $(document).ready(function () {
    if ($(".has-zoom-animation").length > 0) {
      $(".has-zoom-animation").addClass("vc-animated");
    }
  });
})(jQuery);

/***/ }),

/***/ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js":
/*!*****************************************************!*\
  !*** ./src/js/blocks/_wp-block-vc-blocks-vctabs.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function ($) {
  //tabs
  $(".wp-block-vc-blocks-vctab").click(function () {
    var tabPane = $(this).attr("data-vc-target");
    $(".vc-tab-pane").removeClass("active").removeClass("show");
    $(tabPane).addClass("active").addClass("show");
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
  }); //tabs dropdown

  if ($(document).width() < 992) {
    var tabs = {};
    var tabsEl = $(".vctabs");

    if (tabsEl) {
      var tabsWrapperEl = tabsEl.find(".vctabs-wrapper");
      var activeTab = "";
      var tabsOptions = "";

      if (tabsWrapperEl.length) {
        var pElements = $(tabsWrapperEl).children(".vctab");
        pElements.each(function () {
          var tabText = $(this).find("p").text();
          tabs[$(this).attr("data-vc-target")] = tabText;

          if (activeTab === "" && $(this).hasClass("active")) {
            activeTab = tabText;
          }
        });
        console.log(tabs);
        debugger;

        if (tabs) {
          $.each(tabs, function (k, v) {
            tabsOptions += "<li><a class=\"dropdown-item\" data-vc-target=\"".concat(k, "\" href=\"javascript:;\">").concat(v, "</a></li>");
          });
          tabsEl.after("<div class=\"dropdown mb-5\">\n  <button class=\"btn btn-secondary dropdown-toggle\" type=\"button\" id=\"dropdownMenuVctabs\" data-bs-toggle=\"dropdown\" aria-expanded=\"false\">\n".concat(activeTab, "\n  </button>\n  <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenuVctabs\" id=\"dropdownMenuVctabsMenu\">\n   ").concat(tabsOptions, "\n  </ul>\n</div>")); //remove deskto ptabs

          $(tabsEl).remove();
        } //bind click event


        $("#dropdownMenuVctabsMenu a").on("click", function () {
          var tabId = $(this).attr("data-vc-target");
          var tabTitle = $(this).text(); //hide all tab panes

          $(".vc-tab-pane").removeClass("active").removeClass("show"); //show clicked tab pane

          $(tabId).addClass("active").addClass("show"); //change dropdown text

          $("#dropdownMenuVctabs").html(tabTitle);
        });
      }
    }
  }
})(jQuery);

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocks/_wp-block-vc-blocks-vctabs */ "./src/js/blocks/_wp-block-vc-blocks-vctabs.js");
/* harmony import */ var _blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_vc_blocks_vctabs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocks/_wp-block-cover */ "./src/js/blocks/_wp-block-cover.js");
/* harmony import */ var _blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_blocks_wp_block_cover__WEBPACK_IMPORTED_MODULE_1__);



(function ($) {
  $(document).scroll(function () {
    var $nav = $(".navbar.fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
})(jQuery);

/***/ }),

/***/ "./src/script.js":
/*!***********************!*\
  !*** ./src/script.js ***!
  \***********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scss/index.scss */ "./src/scss/index.scss");
/* harmony import */ var _scss_index_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_index_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _js_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./js/index */ "./src/js/index.js");



/***/ }),

/***/ "./src/scss/index.scss":
/*!*****************************!*\
  !*** ./src/scss/index.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2Jsb2Nrcy9fd3AtYmxvY2stY292ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2Jsb2Nrcy9fd3AtYmxvY2stdmMtYmxvY2tzLXZjdGFicy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL3NjcmlwdC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvc2Nzcy9pbmRleC5zY3NzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5IiwibGVuZ3RoIiwiYWRkQ2xhc3MiLCJqUXVlcnkiLCJjbGljayIsInRhYlBhbmUiLCJhdHRyIiwicmVtb3ZlQ2xhc3MiLCJzaWJsaW5ncyIsIndpZHRoIiwidGFicyIsInRhYnNFbCIsInRhYnNXcmFwcGVyRWwiLCJmaW5kIiwiYWN0aXZlVGFiIiwidGFic09wdGlvbnMiLCJwRWxlbWVudHMiLCJjaGlsZHJlbiIsImVhY2giLCJ0YWJUZXh0IiwidGV4dCIsImhhc0NsYXNzIiwiY29uc29sZSIsImxvZyIsImsiLCJ2IiwiYWZ0ZXIiLCJyZW1vdmUiLCJvbiIsInRhYklkIiwidGFiVGl0bGUiLCJodG1sIiwic2Nyb2xsIiwiJG5hdiIsInRvZ2dsZUNsYXNzIiwic2Nyb2xsVG9wIiwiaGVpZ2h0Il0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkEsQ0FBQyxVQUFVQSxDQUFWLEVBQWE7QUFDVkEsR0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzFCLFFBQUlGLENBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCRyxNQUF6QixHQUFrQyxDQUF0QyxFQUF5QztBQUNyQ0gsT0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJJLFFBQXpCLENBQWtDLGFBQWxDO0FBQ0g7QUFDSixHQUpEO0FBS0gsQ0FORCxFQU1HQyxNQU5ILEU7Ozs7Ozs7Ozs7O0FDQUEsQ0FBQyxVQUFVTCxDQUFWLEVBQWE7QUFDVjtBQUNBQSxHQUFDLENBQUMsMkJBQUQsQ0FBRCxDQUErQk0sS0FBL0IsQ0FBcUMsWUFBWTtBQUU3QyxRQUFNQyxPQUFPLEdBQUdQLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsSUFBUixDQUFhLGdCQUFiLENBQWhCO0FBQ0FSLEtBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JTLFdBQWxCLENBQThCLFFBQTlCLEVBQXdDQSxXQUF4QyxDQUFvRCxNQUFwRDtBQUNBVCxLQUFDLENBQUNPLE9BQUQsQ0FBRCxDQUFXSCxRQUFYLENBQW9CLFFBQXBCLEVBQThCQSxRQUE5QixDQUF1QyxNQUF2QztBQUVBSixLQUFDLENBQUMsSUFBRCxDQUFELENBQVFVLFFBQVIsR0FBbUJELFdBQW5CLENBQStCLFFBQS9CO0FBQ0FULEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUUksUUFBUixDQUFpQixRQUFqQjtBQUVILEdBVEQsRUFGVSxDQWFWOztBQUNBLE1BQUlKLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlVLEtBQVosS0FBc0IsR0FBMUIsRUFBK0I7QUFDM0IsUUFBSUMsSUFBSSxHQUFHLEVBQVg7QUFDQSxRQUFNQyxNQUFNLEdBQUdiLENBQUMsQ0FBQyxTQUFELENBQWhCOztBQUNBLFFBQUlhLE1BQUosRUFBWTtBQUNSLFVBQU1DLGFBQWEsR0FBR0QsTUFBTSxDQUFDRSxJQUFQLENBQVksaUJBQVosQ0FBdEI7QUFDQSxVQUFJQyxTQUFTLEdBQUcsRUFBaEI7QUFDQSxVQUFJQyxXQUFXLEdBQUcsRUFBbEI7O0FBQ0EsVUFBSUgsYUFBYSxDQUFDWCxNQUFsQixFQUEwQjtBQUN0QixZQUFJZSxTQUFTLEdBQUdsQixDQUFDLENBQUNjLGFBQUQsQ0FBRCxDQUFpQkssUUFBakIsQ0FBMEIsUUFBMUIsQ0FBaEI7QUFDQUQsaUJBQVMsQ0FBQ0UsSUFBVixDQUFlLFlBQVk7QUFDdkIsY0FBSUMsT0FBTyxHQUFHckIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZSxJQUFSLENBQWEsR0FBYixFQUFrQk8sSUFBbEIsRUFBZDtBQUNBVixjQUFJLENBQUNaLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUVEsSUFBUixDQUFhLGdCQUFiLENBQUQsQ0FBSixHQUF1Q2EsT0FBdkM7O0FBQ0EsY0FBSUwsU0FBUyxLQUFLLEVBQWQsSUFBb0JoQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVF1QixRQUFSLENBQWlCLFFBQWpCLENBQXhCLEVBQW9EO0FBQ2hEUCxxQkFBUyxHQUFHSyxPQUFaO0FBQ0g7QUFDSixTQU5EO0FBT0FHLGVBQU8sQ0FBQ0MsR0FBUixDQUFZYixJQUFaO0FBQ0E7O0FBQ0EsWUFBSUEsSUFBSixFQUFVO0FBQ05aLFdBQUMsQ0FBQ29CLElBQUYsQ0FBT1IsSUFBUCxFQUFhLFVBQVVjLENBQVYsRUFBYUMsQ0FBYixFQUFnQjtBQUN6QlYsdUJBQVcsOERBQW9EUyxDQUFwRCxzQ0FBOEVDLENBQTlFLGNBQVg7QUFDSCxXQUZEO0FBSUFkLGdCQUFNLENBQUNlLEtBQVAsZ01BRWxCWixTQUZrQixvSUFLZkMsV0FMZSx3QkFMTSxDQWNOOztBQUNBakIsV0FBQyxDQUFDYSxNQUFELENBQUQsQ0FBVWdCLE1BQVY7QUFDSCxTQTNCcUIsQ0E4QnRCOzs7QUFDQTdCLFNBQUMsQ0FBQywyQkFBRCxDQUFELENBQStCOEIsRUFBL0IsQ0FBa0MsT0FBbEMsRUFBMkMsWUFBWTtBQUNuRCxjQUFJQyxLQUFLLEdBQUcvQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFRLElBQVIsQ0FBYSxnQkFBYixDQUFaO0FBQ0EsY0FBSXdCLFFBQVEsR0FBR2hDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXNCLElBQVIsRUFBZixDQUZtRCxDQUduRDs7QUFDQXRCLFdBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JTLFdBQWxCLENBQThCLFFBQTlCLEVBQXdDQSxXQUF4QyxDQUFvRCxNQUFwRCxFQUptRCxDQUtuRDs7QUFDQVQsV0FBQyxDQUFDK0IsS0FBRCxDQUFELENBQVMzQixRQUFULENBQWtCLFFBQWxCLEVBQTRCQSxRQUE1QixDQUFxQyxNQUFyQyxFQU5tRCxDQU9uRDs7QUFDQUosV0FBQyxDQUFDLHFCQUFELENBQUQsQ0FBeUJpQyxJQUF6QixDQUE4QkQsUUFBOUI7QUFDSCxTQVREO0FBVUg7QUFDSjtBQUNKO0FBR0osQ0FuRUQsRUFtRUczQixNQW5FSCxFOzs7Ozs7Ozs7Ozs7QUNBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTs7QUFFQSxDQUFDLFVBQVVMLENBQVYsRUFBYTtBQUNWQSxHQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZaUMsTUFBWixDQUFtQixZQUFZO0FBQzNCLFFBQU1DLElBQUksR0FBR25DLENBQUMsQ0FBQyxtQkFBRCxDQUFkO0FBQ0FtQyxRQUFJLENBQUNDLFdBQUwsQ0FBaUIsVUFBakIsRUFBNkJwQyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQyxTQUFSLEtBQXNCRixJQUFJLENBQUNHLE1BQUwsRUFBbkQ7QUFDSCxHQUhEO0FBSUgsQ0FMRCxFQUtHakMsTUFMSCxFOzs7Ozs7Ozs7Ozs7QUNIQztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7QUNBRCx1QyIsImZpbGUiOiJzY3JpcHQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9zY3JpcHQuanNcIik7XG4iLCIoZnVuY3Rpb24gKCQpIHtcclxuICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJChcIi5oYXMtem9vbS1hbmltYXRpb25cIikubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAkKFwiLmhhcy16b29tLWFuaW1hdGlvblwiKS5hZGRDbGFzcyhcInZjLWFuaW1hdGVkXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59KShqUXVlcnkpOyIsIihmdW5jdGlvbiAoJCkge1xyXG4gICAgLy90YWJzXHJcbiAgICAkKFwiLndwLWJsb2NrLXZjLWJsb2Nrcy12Y3RhYlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcblxyXG4gICAgICAgIGNvbnN0IHRhYlBhbmUgPSAkKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKTtcclxuICAgICAgICAkKFwiLnZjLXRhYi1wYW5lXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpLnJlbW92ZUNsYXNzKFwic2hvd1wiKTtcclxuICAgICAgICAkKHRhYlBhbmUpLmFkZENsYXNzKFwiYWN0aXZlXCIpLmFkZENsYXNzKFwic2hvd1wiKTtcclxuXHJcbiAgICAgICAgJCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgLy90YWJzIGRyb3Bkb3duXHJcbiAgICBpZiAoJChkb2N1bWVudCkud2lkdGgoKSA8IDk5Mikge1xyXG4gICAgICAgIGxldCB0YWJzID0ge307XHJcbiAgICAgICAgY29uc3QgdGFic0VsID0gJChcIi52Y3RhYnNcIik7XHJcbiAgICAgICAgaWYgKHRhYnNFbCkge1xyXG4gICAgICAgICAgICBjb25zdCB0YWJzV3JhcHBlckVsID0gdGFic0VsLmZpbmQoXCIudmN0YWJzLXdyYXBwZXJcIik7XHJcbiAgICAgICAgICAgIGxldCBhY3RpdmVUYWIgPSBcIlwiO1xyXG4gICAgICAgICAgICBsZXQgdGFic09wdGlvbnMgPSBcIlwiO1xyXG4gICAgICAgICAgICBpZiAodGFic1dyYXBwZXJFbC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIGxldCBwRWxlbWVudHMgPSAkKHRhYnNXcmFwcGVyRWwpLmNoaWxkcmVuKFwiLnZjdGFiXCIpO1xyXG4gICAgICAgICAgICAgICAgcEVsZW1lbnRzLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCB0YWJUZXh0ID0gJCh0aGlzKS5maW5kKFwicFwiKS50ZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGFic1skKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKV0gPSB0YWJUZXh0O1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhY3RpdmVUYWIgPT09IFwiXCIgJiYgJCh0aGlzKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY3RpdmVUYWIgPSB0YWJUZXh0O1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGFicyk7XHJcbiAgICAgICAgICAgICAgICBkZWJ1Z2dlclxyXG4gICAgICAgICAgICAgICAgaWYgKHRhYnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAkLmVhY2godGFicywgZnVuY3Rpb24gKGssIHYpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFic09wdGlvbnMgKz0gYDxsaT48YSBjbGFzcz1cImRyb3Bkb3duLWl0ZW1cIiBkYXRhLXZjLXRhcmdldD1cIiR7a31cIiBocmVmPVwiamF2YXNjcmlwdDo7XCI+JHt2fTwvYT48L2xpPmA7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRhYnNFbC5hZnRlcihgPGRpdiBjbGFzcz1cImRyb3Bkb3duIG1iLTVcIj5cclxuICA8YnV0dG9uIGNsYXNzPVwiYnRuIGJ0bi1zZWNvbmRhcnkgZHJvcGRvd24tdG9nZ2xlXCIgdHlwZT1cImJ1dHRvblwiIGlkPVwiZHJvcGRvd25NZW51VmN0YWJzXCIgZGF0YS1icy10b2dnbGU9XCJkcm9wZG93blwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiPlxyXG4ke2FjdGl2ZVRhYn1cclxuICA8L2J1dHRvbj5cclxuICA8dWwgY2xhc3M9XCJkcm9wZG93bi1tZW51XCIgYXJpYS1sYWJlbGxlZGJ5PVwiZHJvcGRvd25NZW51VmN0YWJzXCIgaWQ9XCJkcm9wZG93bk1lbnVWY3RhYnNNZW51XCI+XHJcbiAgICR7dGFic09wdGlvbnN9XHJcbiAgPC91bD5cclxuPC9kaXY+YCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vcmVtb3ZlIGRlc2t0byBwdGFic1xyXG4gICAgICAgICAgICAgICAgICAgICQodGFic0VsKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICAgICAgLy9iaW5kIGNsaWNrIGV2ZW50XHJcbiAgICAgICAgICAgICAgICAkKFwiI2Ryb3Bkb3duTWVudVZjdGFic01lbnUgYVwiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGFiSWQgPSAkKHRoaXMpLmF0dHIoXCJkYXRhLXZjLXRhcmdldFwiKTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgdGFiVGl0bGUgPSAkKHRoaXMpLnRleHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAvL2hpZGUgYWxsIHRhYiBwYW5lc1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIudmMtdGFiLXBhbmVcIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIikucmVtb3ZlQ2xhc3MoXCJzaG93XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vc2hvdyBjbGlja2VkIHRhYiBwYW5lXHJcbiAgICAgICAgICAgICAgICAgICAgJCh0YWJJZCkuYWRkQ2xhc3MoXCJhY3RpdmVcIikuYWRkQ2xhc3MoXCJzaG93XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vY2hhbmdlIGRyb3Bkb3duIHRleHRcclxuICAgICAgICAgICAgICAgICAgICAkKFwiI2Ryb3Bkb3duTWVudVZjdGFic1wiKS5odG1sKHRhYlRpdGxlKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbn0pKGpRdWVyeSk7IiwiaW1wb3J0IFwiLi9ibG9ja3MvX3dwLWJsb2NrLXZjLWJsb2Nrcy12Y3RhYnNcIjtcclxuaW1wb3J0IFwiLi9ibG9ja3MvX3dwLWJsb2NrLWNvdmVyXCI7XHJcblxyXG4oZnVuY3Rpb24gKCQpIHtcclxuICAgICQoZG9jdW1lbnQpLnNjcm9sbChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgY29uc3QgJG5hdiA9ICQoXCIubmF2YmFyLmZpeGVkLXRvcFwiKTtcclxuICAgICAgICAkbmF2LnRvZ2dsZUNsYXNzKCdzY3JvbGxlZCcsICQodGhpcykuc2Nyb2xsVG9wKCkgPiAkbmF2LmhlaWdodCgpKTtcclxuICAgIH0pO1xyXG59KShqUXVlcnkpOyIsIiBpbXBvcnQgJy4vc2Nzcy9pbmRleC5zY3NzJztcclxuaW1wb3J0ICcuL2pzL2luZGV4JzsiLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9