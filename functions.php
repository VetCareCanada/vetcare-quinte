<?php

$template_path = get_stylesheet_directory();
$include_files = [
    '/inc/template-tags.php', // Custom template tags for the theme.
    '/libs/theme-setup.php',
];

if ($include_files) {
    foreach ($include_files as $key => $value) {
        if (file_exists($template_path . $value)) {
            require_once($template_path . $value);
        }
    }
}

if (! function_exists("vetcare_scripts")) {

    function vetcare_scripts()
    {
        // enqueue style
        wp_enqueue_style(
            'vetcare-style',
            get_stylesheet_directory_uri() . '/dist/style.css',
            array(),
            wp_get_theme()->get('Version')
        );

        // enqueue script
        wp_enqueue_script(
            'bootstrap-script',
            'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js',
            wp_get_theme()->get('Version'),
            true
        );
        wp_enqueue_script(
            'vetcare-script',
            get_stylesheet_directory_uri() . '/dist/script.js',
            array( 'jquery', 'bootstrap-script' ),
            wp_get_theme()->get('Version'),
            true
        );
    }
}

add_action('wp_enqueue_scripts', 'vetcare_scripts');

//enqueue theme style in editor
//echo get_stylesheet_directory_uri() . '/dist/style.css';
add_editor_style(get_stylesheet_directory_uri() . '/dist/style.css');

//enqueue editor style
//add_editor_style(get_stylesheet_directory_uri()."/assets/dist/css/editor.css");


/**
 * Add poppins font to theme editor
 */
add_action('enqueue_block_editor_assets', function () {
    wp_enqueue_style('vetcare-poppins', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&family=Poppins:wght@700&display=swap');
});
