<?php
/**
 * Functions and definitions
 */

if (! function_exists('vetcare_child_theme_setup')) {
    function vetcare_child_theme_setup()
    {
        add_theme_support(
            'editor-color-palette',
            array(
                array(
                    'name'  => esc_html__('Primary', 'vetcare'),
                    'slug'  => 'primary',
                    'color' => '#008e91',
                ),
                array(
                    'name'  => esc_html__('Secondary', 'vetcare'),
                    'slug'  => 'secondary',
                    'color' => '#333333',
                ),
                array(
                    'name'  => esc_html__('Tertiary', 'vetcare'),
                    'slug'  => 'tertiary',
                    'color' => '#FFE1B2',
                ),
                array(
                    'name'  => esc_html__('Dark', 'vetcare'),
                    'slug'  => 'dark',
                    'color' => '#1A1A1A',
                ),
                 array(
                    'name'  => esc_html__('White', 'vetcare'),
                    'slug'  => 'white',
                    'color' => '#fff',
                ),

            )
        );
    }
}

add_action('after_setup_theme', 'vetcare_child_theme_setup');
